package main_programs;


import boolean_model.*;
public class Test_program {

	public static void main(String[] args) {
		
Truth a = new Truth("A",true);
Truth b = new Truth("B",true);
Truth c = new Truth ("C",false);
Truth d = new Truth("D",true);
Truth e = new Truth("E",false);
Truth f = new Truth("F",true);

//NOTICE. DO NOT INSTANTIATED OPERATION TYPE. 
	And relation1 = new And(a,b);
	Nand relation2 = new Nand(e,f);
	And relation3 = new And(relation1,c);
	
	Nor relation4 = new Nor(d,relation2);
	Xor main = new Xor(relation3,relation4);
	
	
System.out.println(main.evaluate().getInfo());
System.out.println("involved: ");
for(Placeholder o: main.getInvolved())System.out.println(o.getInfo());

Sentential_Belief_System sbs = new Sentential_Belief_System();
sbs.addRule(relation4);

	}

}
