package boolean_model;

public class Not extends Operation{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Not(Represented a) {
		super(a);
		super.relation="Not";
		// TODO Auto-generated constructor stub
		
	}
@Override
protected Truth recfunc(){
	if(a instanceof Operation){a=((Operation)a).evaluate();
	return evaluate();
	}
		return new Truth(!((Truth)a).value,((Truth)a).description);
	
	
}
@Override
protected boolean operation(Truth t1, Truth t2) {
	// not to be used.
	return false;
}
}
