package boolean_model;

public class And extends Operation{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public And(Represented a, Represented b) {
		super(a, b);
		super.relation="And";
		// TODO Auto-generated constructor stub
	}

	protected boolean operation(Truth t1, Truth t2){return t1.value && t2.value;}

}
