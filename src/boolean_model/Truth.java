package boolean_model;

import java.util.HashSet;

public class Truth extends Placeholder{
	/**
	 * 
	 */
	private HashSet<Placeholder> involved;//All the other truths that were involved in the evaluation that leads up to this truth.
	//This does count itself;
	private static final long serialVersionUID = 1L;
	public Truth(boolean value){
		super("");
		involved = new HashSet<Placeholder>();
		involved.add(this);
		this.value=value;}
	public Truth(boolean value, String description){
		super(description);
		involved = new HashSet<Placeholder>();
		involved.add(this);
		this.value=value;
		
	}
	public Truth( String description, boolean value){
		this(value,description);
	}
	public Truth(String description, boolean value, HashSet<Placeholder>involved){
		this(value,description);
		this.involved.addAll(involved);
	}
	boolean value;

	
@Override
	public String getInfo(){
	
		return "description: "+super.description+", value: "+value;
	}
	@Override
	public HashSet<Placeholder> getInvolved(){
		return involved;
	}
	

}
