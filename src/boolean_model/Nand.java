package boolean_model;

public class Nand extends Operation {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Nand(Represented a, Represented b) {
		super(a, b);
		super.relation="Nand";
		// TODO Auto-generated constructor stub
	}

	protected boolean operation(Truth t1, Truth t2){return !(t1.value && t2.value);}

}

