package boolean_model;

public class Xor extends Operation{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public Xor(Represented a, Represented b) {
		super(a, b);
		super.relation="Xor";
		// TODO Auto-generated constructor stub
	}
@Override
protected boolean operation(Truth t1, Truth t2){return Boolean.logicalXor(t1.value, t2.value);}

}

