package boolean_model;

import java.io.Serializable;

import java.util.HashSet;

public abstract class Operation implements Represented,Serializable {
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private HashSet<Placeholder> involved;
	Represented a;
	Represented b;
	protected String relation;
	private final String startDescription="";
	
	public Operation(Represented a, Represented b){
		this.a=a;
		this.b=b;
		involved = new HashSet<Placeholder>();
		System.out.println(involved);
		involved.addAll(a.getInvolved());
		involved.addAll(b.getInvolved());
		
	}
	public Operation(Represented a){
		this.a=a;
		//This is only for the NOT operator.
	}

	@Override
	public HashSet<Placeholder> getInvolved(){
		return this.involved;
	
	}
	
		
		protected abstract boolean operation(Truth t1, Truth t2);
		
		public Truth evaluate(){//To evaluate with placeholders, all involved placeholders must be asserted.
			//TODO correct this type check. Try.
			//if(!(a instanceof Truth && b instanceof Truth) )return null;
			//return recfunc(this.startDescription);
			try{
			return recfunc2(this,startDescription);}
			catch(NullPointerException npe){
				return null;
			}
			//Startdescription is any string that is desired to be outputted.
			
		}
		
		private void entered(String string){
			try{Thread.sleep(500);
			
			}catch(InterruptedException ie){
				ie.printStackTrace();
			}
		string = " entered : "+string;
		System.out.println(string);}
		
		protected Truth recfunc2(Operation current,String string){
			if(current.a instanceof Operation && current.b instanceof Operation){
				entered("Op-Op.");
				
				Truth t1 = recfunc2(((Operation)current.a),string);
				Truth t2 = recfunc2(((Operation)current.b),string);
				return new Truth(current.operation(t1, t2),"("+t1.description+" "+current.relation+" "+t2.description+")");
			}
			
			
			else if(current.a instanceof Operation && current.b instanceof Truth){
				entered("Op-truth.");
				Truth t1 = recfunc2(((Operation)current.a),string);
				return new Truth(current.operation((Truth)current.b, t1),("("+t1.description+" "+current.relation+" "+((Truth)current.b).description)+")");
				
			}
			
			else if(current.b instanceof Operation && current.a instanceof Truth){
				entered("truth-op.");
				Truth t2 = recfunc2(((Operation)current.b),string);
				return new Truth(current.operation((Truth)current.a, t2),("("+((Truth)current.a).description+" "+current.relation+" "+t2.description+")"));
				
			}
			else //(a instanceof Truth && b instanceof Truth )
				entered("truth-truth.");
		
				return new Truth(operation((Truth)current.a,(Truth)current.b),"("+((Truth)current.a).description+" "+ current.relation+" "+((Truth)current.b).description+")"); 
			
			
		}
			
		protected Truth recfunc(){
			//TODO fix this. Wait. Do we need this?
			if(a instanceof Truth && b instanceof Truth )return new Truth(operation((Truth)a,(Truth)b),((Truth)a).description+" "+ relation+" "+((Truth)b).description); 
			
			else if(a instanceof Operation && b instanceof Truth){
				return ((Operation)a).recfunc();
				//return recfunc(startDescription);
			//	return new Truth(operation(((Operation)a).evaluate(),(Truth)b));
			}
			else if(b instanceof Operation && a instanceof Truth){
				//startDescription = startDescription+" "+ ((Truth)a).description+" "+relation;
				//b=
				return ((Operation)b).recfunc();
				//return recfunc();
				//return new Truth(operation(((Operation)b).evaluate(),(Truth)a));
			}else{//when both are operations,
				//These two are ok.
				a=((Operation)a).recfunc();
				//startDescription = startDescription +" "+relation+" ";
				b=((Operation)b).recfunc();
				return recfunc();
				
				
				
			}
		}
		
	
}