package boolean_model;

import java.io.Serializable;
import java.util.HashSet;

public class Placeholder  implements Serializable,Represented{
/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
protected String description;
protected Truth assertTruth(boolean value){
	//This method is used to temporary assign a placeholder a valid boolean value.
	return new Truth(value,description);
}
public Placeholder(String description){this.description=description;}

 public String getInfo(){return description;}
 
@Override
public HashSet<Placeholder> getInvolved() {
	// TODO Auto-generated method stub
	return null;
}
}
