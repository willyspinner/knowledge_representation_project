package boolean_model;

public class Nor extends Operation{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Nor(Represented a, Represented b) {
		super(a, b);
		super.relation="Nor";
		// TODO Auto-generated constructor stub
	}

	protected boolean operation(Truth t1, Truth t2){return !(t1.value || t2.value);}

}
