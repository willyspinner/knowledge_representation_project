package boolean_model;

public class Or extends Operation{
	
	public Or(Represented a, Represented b) {
		super(a, b);
		super.relation="Or";
		// TODO Auto-generated constructor stub
	}

	protected boolean operation(Truth t1, Truth t2){return t1.value || t2.value;}

}